# Text Input 文本框

文本输入框。

### 何时使用

需要手动输入文字使用。

### 基本用法

<h4>Default</h4>
<d-text-input placeholder="Please Enter" id="textInput"></d-text-input>
<h4>Disabled</h4>
<d-text-input placeholder="Please Enter" :disabled="true"></d-text-input>
<h4>Error</h4>
<d-text-input placeholder="Please Enter" :error="true"></d-text-input>
