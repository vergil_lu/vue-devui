import sidebar from './sidebar'
import head from './head'
import nav from './nav'

const config = {
  title: "Vue DevUI",
  description: "Vue DevUI 组件库",
  head,
  themeConfig: {
    sidebar,
    nav,
    logo: '../../assets/logo.svg'
  },
};

export default config;
